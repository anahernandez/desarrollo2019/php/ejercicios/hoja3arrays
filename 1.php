<?php
/** 
    *
    * Ésta función genera una serie de números aleatorios 
    * @param int $vmin Este es el valor mínimo
    * @param int $vmax Este es el valor máximo
    * @param int $num Número de valores a generar
    * @return int[] El conjunto de números solicitados
    * 
    */

function ejercicio1($vmin,$vmax,$num){
    /** 
    * Ésta función genera una serie de números aleatorios 
    * @param type $vmin Este es el valor mínimo
    * @param type $vmax Este es el valor máximo
    * @param type $num Número de valores a generar
    * @return array El conjunto de números solicitados
    */

   $miarray=array();
    for ($i = 0; $i <$num; $i++) {
      $aleatorio=random_int($vmin,$vmax);
      array_push($miarray,$aleatorio);
        } 
        
     return $miarray;
}

$salida=ejercicio1(1,5,2);
var_dump($salida);
?>

