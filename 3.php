<?php

/**
 * 
 * @param int $num El número de colores a generar
 * @return array los colores solicitados en un array de cadenas
 */

function generaColores($num){
    $colores=array();
    for($i=0; $i<$num;$i++){
        $colores[$i]="#";
        for ($j=0;$j<$num;$j++){
            $colores[$i].= dechex(mt_rand(0,15));
        }
    }
        return $colores;
            
}

var_dump($salida=generaColores(5));