<?php

/**
 * Función que genera colores
 * @param int $num El número de colores a generar
 * @param booleano con valor true nos indica que coloquemos la almohadilla
 * @return array Los colores solicitados en un array de cadenas
 */

function generaColores($num,$opcional){
    
    $colores=array();
    
    for($i=0; $i<$num;$i++){
        
        if($opcional){
            
        $colores[$i]="#";
        }
        
        for ($j=0;$j<$num;$j++){
            
            $colores[$i].= dechex(mt_rand(0,15));
        }
        
       } 

        return $colores;          
}
$opcional=true;
$salida=generaColores(5,$opcional);
var_dump($salida);

